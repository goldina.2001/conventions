class DateRange(val start: MyDate, val end: MyDate):Iterable<MyDate>{
    override fun iterator():Iterator<MyDate> {
        return object : Iterator<MyDate> {
            var temp: MyDate=start
            override fun next():MyDate{
                if(!hasNext()) throw NoSuchElementException()
                val result = temp
                temp = temp.followingDate()
                return  result
            }
            override fun hasNext():Boolean = temp<=end
    	}
	}
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in firstDate..secondDate) {
        handler(date)
    }
}
